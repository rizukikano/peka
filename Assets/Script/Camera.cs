﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

// Show WebCams and Microphones on an iPhone/iPad.
// Make sure NSCameraUsageDescription and NSMicrophoneUsageDescription
// are in the Info.plist.

public class Camera : MonoBehaviour
{
    public RawImage Image;
    public RawImage Image2;
    public Quaternion baseRotation;
    public WebCamTexture webcamTexture;
    IEnumerator Start()
    {
        findWebCams();

        yield return Application.RequestUserAuthorization(UserAuthorization.WebCam);
        if (Application.HasUserAuthorization(UserAuthorization.WebCam))
        {
            Debug.Log("webcam found");
        }
        else
        {
            Debug.Log("webcam not found");
        }
        webcamTexture = new WebCamTexture();
        baseRotation = transform.rotation;
        Image.texture = webcamTexture;
        Image2.texture = webcamTexture;
        webcamTexture.Play();
    }

    void findWebCams()
    {
        foreach (var device in WebCamTexture.devices)
        {
            Debug.Log("Name: " + device.name);
        }
    }
    void Update()
    {
        transform.rotation = baseRotation * Quaternion.AngleAxis(webcamTexture.videoRotationAngle, Vector2.up);
    }


}